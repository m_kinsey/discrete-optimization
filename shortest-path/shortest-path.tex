% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options
\usepackage{amsthm}

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
%\usepackage{sectsty}
%\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%%% END Article customizations

%%% The "real" document content comes below...

\title{Finding Shortest Paths}
\author{Michael Kinsey}
\date{4 April 2017}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed
\newtheorem{theorem}{Theorem}
\newtheorem{algo}{Algorithm}

\begin{document}
\maketitle

The shortest path problem can be described as finding the cheapest path that
travels from node $x$ to node $y$ in the graph $G$ with weighted edges.

\section{Brute Force}

In order to solve the shortest path problem by brute force, we will need to
check possible path from $x$ to $y$. This can be be thought of as every possible
combination of nodes between $x$ and $y$. This is bounded by {(n-2)!} where $n$
is the number of nodes in $G$.

\section{Greedy Algorithm}
A potential greedy algorithm could be constructed as follows:

Starting at $x$, add the shortest outgoing path from the current node without
creating a cycle. The connected node is now the current node. Continue in this
fashion until node $y$ is reached.

This greedy solution is fast and may have good performance, but it is not
guaranteed to be correct. In fact, it is trivial to construct a graph in which
this algorithm has terrible performance, such as figure 1 below.

\includegraphics[width=\textwidth]{greedy-path.png}

The greedy solution will first take the edge to $D$, and then be forced to take
the most expensive edge when any alternative path would be optimal.

\section{Following Dijkstra's Algorithm}

\begin{algo}
    Given a graph G with non-negative costs assigned to each edge find a minimum
    cost path from node x to y.
    \begin{enumerate}
        \item Assing a temporary cost of 0 to x and $\infty$ to all other nodes in
            G. Set x to be the ``current node'' C.
        \item For each unchecked neighbor z of C, compute the cost on C plus the
            cost of traveling along the edge from C to the beighbor. Compare
            this value to the temporary cost on z. If it is smaller, then it
            becomesthe new temporary cost on z.
        \item Now C is checked and will not be updated again.
        \item If we checked the end node y in the previous step, then we are
            done and the temporary cost on y is the value of the minimum cost
            bath from x to y.
        \item If we have not yet checked y, then among all unchecked nodes
            set the current node C to be that node with the smallest temporary
            cost. Return to step 2.
    \end{enumerate}
\end{algo}

Applying Dijsktra's Algorithm on the provided graph gives the following minimum
cost path:

\[2+1+5+2+3=13\]

\section{Modifying Disjkstra's Algorithm}

In order to record the path taken from $x$ to $y$, modify the algorithm in the
following way:

At step one give all nodes a unique label.

We will keep track of checked nodes in list $l$. At step five before recording
the label of the current node $C$, look at the last node in $l$. If the previous
node is a neighbor to $C$, add $C$ to the list. If not, remove the last node of
the list until it is a neighbor of $C$. This ensures the resulting list will be
a path in $G$ and the most direct path to $y$.

\section{Proving Correctness}

\begin{theorem}
    At each step, the cost on a node v that has been checked is the minimum cost
    of a path from x to v. In particular, when y is checked we have the minimum
    cost from x to y.
\end{theorem}

\begin{proof}
    Base Case: When the set of visited nodes only includes $x$, we can be sure
    that the distance from $x$ to $x$ is 0.\\

    Inductive hypothesis: The cost of the last node that has been checked
    ($c(z)$) is the minimum cost from $x$.

    Assume the inductive hypothesis does not hold, and the minimum cost is
    greater than the one calculated by Dijkstra. Let $ab$ be the first edge in
    the minimum cost path that is not in the path calculated by Dijkstra.
    Then, $c(a) + ab$ must be less than $c(y)$. Similarly, the calculated cost
    $c(b)$ must be less than or equal to $c(a) + ab$ or this edge would not be
    included. Because our algorithm picked $z$ rather than $b$, we have
    $c(z) < c(b)$. By these inequalities, we have that $c(z)  < c(z)$. This
    presents a contradiction, so no alternative path should be shorter than $c(z)$.
\end{proof}

\section{Analysis}
Step 2 of Algorithm 1 performes two simple operations (computing the sum and
comparing two values). At most how many times (in terms of the number of nodes n)
are these two operations performed when running Algorithm 1?\\

In the worst case, we would evauluate this pair of operations for every
remaining unchecked node for every checked node. That is $n-1$ pairs of
operations when $C = x$, and $n - 2$ pairs at the following $C$ and so on.
This makes for a total $(n-1)!$ pairs of operations in the worst case.

\section{Negative Edges}

Algorithm 1 may fail if edges are allowed to have negative edge weights.\\

When Dijkstra's Algorithm reaches $y$, we know that we are done becuase any
alternative path to $y$ would cost the same (with cost 0 edges) or be more
expensive (any positive cost edge). With negative edges, a path to one of $y$'s
neighbors could be initially more expensive, but may be offset by a negative
costing edge. The figure below illustrates such an example.

\includegraphics[width=\textwidth]{sp7}

This simple example illustrates why Dijkstra's Algorithm would not work with
negative edges. Starting at $x$, we take the cheapest path right to $y$ and
according to the algorithm we have found the shortest path, despite the detour
to $b$ actually being shorter.
\end{document}
